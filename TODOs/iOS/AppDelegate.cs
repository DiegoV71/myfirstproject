﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Xamarin.Forms.Platform.iOS;

namespace TODOs.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();
            UINavigationBar.Appearance.BarTintColor = Theme.BlueColor.ToUIColor();
            UINavigationBar.Appearance.TintColor = UIColor.White;
			LoadApplication (new App ());
			return base.FinishedLaunching (app, options);
		}
	}
}