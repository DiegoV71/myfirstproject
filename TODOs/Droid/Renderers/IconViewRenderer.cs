﻿using Android.Graphics;
using Android.Widget;
using TODOs;
using TODOs.Android;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(IconView), typeof(IconViewRenderer))]

namespace TODOs.Android
{
    public class IconViewRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            var label = Control; // for example
            Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "fontawesome-webfont.ttf");  // font name specified here
            label.Typeface = font;
        }
    }
}