﻿using Xamarin.Forms;

namespace TODOs
{
    public class IconView : Label
    {
        public IconView()
        {
            FontFamily = Device.OnPlatform(
                "FontAwesome",
                null,
                null
            );
        }


        public string Icon
        {
            get { return Text; }
            set { Text = value; }
        }


    }
}