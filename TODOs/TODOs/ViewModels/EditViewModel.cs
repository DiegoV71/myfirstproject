﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Input;
using Acr.UserDialogs;
using Xamarin.Forms;

namespace TODOs
{
    public class EditViewModel : INotifyPropertyChanged
    {
        string description;
        int typeIndex = 0;
        int statusIndex = 0;
        Color color;

        public EditViewModel(int taskId)
        {
            Manager = DataManager.Instance();
            UI = UserDialogs.Instance;
            GenerateForm();
            UpdateTaskCommand = new Command(UpdateTask);
            DeleteTaskCommand = new Command(DeleteTask);
        }

        public EditViewModel()
        {
            Manager = DataManager.Instance();
            UI = UserDialogs.Instance;
            CreateTaskCommand = new Command(CreateTask);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #region Properties

        public DataManager Manager { get; set; }

        public IUserDialogs UI { get; set; }

        public ICommand CreateTaskCommand { get; protected set; }

        public ICommand UpdateTaskCommand { get; protected set; }

        public ICommand DeleteTaskCommand { get; protected set; }

        public string Description
        {
            get { return description; }

            set {
                if (description != value)
                {
                    description = value;
                    OnPropertyChanged("Description");
                }
            }
        }

        public int TypeIndex
        {
            get { return typeIndex; }

            set {
                if (typeIndex != value)
                {
                    typeIndex = value;
                    OnPropertyChanged("TypeIndex");
                }
            }
        }

        public int StatusIndex
        {
            get { return statusIndex; }

            set {
                if (statusIndex != value)
                {
                    statusIndex = value;
                    OnPropertyChanged("StatusIndex");
                }
            }
        }

        public Color Color
        {
            get { return color; }

            set {
                if (color != value)
                {
                    color = value;
                    OnPropertyChanged("Color");
                }
            }
        }

        #endregion

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        void CreateTask()
        {            
            if (!IsValidForm())
            {
                return;
            }

            var newTask = GenerateTask();
            Debug.WriteLine(newTask.ToString());
            Manager.AddTask(newTask);
            TasksPage.CurrentListView.BeginRefresh();
            App.NaviPage.PopAsync();
        }

        void DeleteTask()
        {
            Manager.DeleteTask(StateMachine.CurrentTask.Id);
            TasksPage.CurrentListView.BeginRefresh();
            App.NaviPage.PopAsync();
        }

        void UpdateTask()
        {
            if (!IsValidForm())
            {
                return;
            }

            var newTask = GenerateTask();
            newTask.Id = StateMachine.CurrentTask.Id;
            Manager.DataBase.Update(newTask);
            TasksPage.CurrentListView.BeginRefresh();
            App.NaviPage.PopAsync();
        }

        Task.Types GetTypeByIndex()
        {
            switch (TypeIndex)
            {
                default:
                    {
                        return Task.Types.none;
                    }

                case 1: 
                    {
                        return Task.Types.bug;
                    }

                case 2: 
                    {
                        return Task.Types.feature;
                    }
            }
        }

        Enum GetStatusByIndex()
        {
            switch (TypeIndex)
            {
                default:
                    {
                        return null;
                    }

                case 1:
                    {
                        if (StatusIndex == 1)
                        {
                            return Task.BugStatuses.newBug;
                        }

                        if (StatusIndex == 2)
                        {
                            return Task.BugStatuses.fixedBug;
                        }

                        return null;
                    }

                case 2:
                    {
                        if (StatusIndex == 1)
                        {
                            return Task.FeatureStatuses.newFeature;
                        }

                        if (StatusIndex == 2)
                        {
                            return Task.FeatureStatuses.Implemented;
                        }

                        return null;
                    }
            }
        }

        Task.Colors GetColor()
        {
            if (Color == Theme.RedColor)
            {
                return Task.Colors.Red;
            }

            if (Color == Theme.GreenColor)
            {
                return Task.Colors.Green;
            }

            if (Color == Theme.BlueColor)
            {
                return Task.Colors.Blue;
            }

            return Task.Colors.none;
        }

        bool IsValidForm()
        {
            var errorList = new AlertConfig()
            {
                Title = "Warning",
                OkText = "Ok",
                Message = string.Empty
            };

            bool isValid = true;
            
            if (Description.Length == 0)
            {
                isValid = false;
                errorList.Message += "\nDescription field is empty.";
            }

            if (Description.Length > 250)
            {
                isValid = false;
                errorList.Message += "\nDescription more than 250 characters.";
            }

            if (TypeIndex == 0)
            {
                isValid = false;
                errorList.Message += "\nType no selected.";
            }

            if (statusIndex == 0)
            {
                isValid = false;
                errorList.Message += "\nStatus no selected.";
            }

            if (!isValid)
            {
                UI.Alert(errorList);
            }

            return isValid;
        }

        Task GenerateTask()
        {
            Task newTask;

            switch (GetTypeByIndex())
            {
                case Task.Types.bug:
                    {
                        newTask = new Bug();
                        ((Bug)newTask).Status = (Task.BugStatuses)GetStatusByIndex();
                        break;
                    }

                case Task.Types.feature:
                    {
                        newTask = new Feature();
                        ((Feature)newTask).Status = (Task.FeatureStatuses)GetStatusByIndex();
                        break;
                    }

                default:
                    {
                        newTask = new Task();
                        break;
                    }
            }

            newTask.Parent = StateMachine.CurrentProject.Id;
            newTask.Description = Description;
            newTask.Color = GetColor();
            return newTask;
        }

        void GenerateForm()
        {
            var currentTask = StateMachine.CurrentTask;

            Description = currentTask.Description;

            SetColorByTask();
            SetTypeByTask();
            SetStatusByTask();
        }

        void SetColorByTask()
        {
            switch (StateMachine.CurrentTask.Color)
            {
                case Task.Colors.Red:
                    {
                        Color = Theme.RedColor;
                        break;
                    }

                case Task.Colors.Green:
                    {
                        Color = Theme.GreenColor;
                        break;
                    }

                case Task.Colors.Blue:
                    {
                        Color = Theme.BlueColor;
                        break;
                    }
            }
        }

        void SetTypeByTask()
        {
            switch (StateMachine.CurrentTask.Type)
            {
                case Task.Types.bug:
                    {
                        Debug.WriteLine("Это баг");
                        TypeIndex = 1;
                        break;
                    }

                case Task.Types.feature:
                    {
                        Debug.WriteLine("Это фича");
                        TypeIndex = 2;
                        break;
                    }
            }
        }

        void SetStatusByTask()
        {
            if ((StateMachine.CurrentTask.TaskStatus == Task.BugStatuses.newBug.ToString()) ||
                (StateMachine.CurrentTask.TaskStatus == Task.FeatureStatuses.newFeature.ToString()))
            {
                statusIndex = 1;
                return;
            }

            if ((StateMachine.CurrentTask.TaskStatus == Task.BugStatuses.fixedBug.ToString()) ||
                (StateMachine.CurrentTask.TaskStatus == Task.FeatureStatuses.Implemented.ToString()))
            {
                statusIndex = 2;
                return;
            }
        }
    }
}