﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;

using Acr.UserDialogs;
using Xamarin.Forms;

namespace TODOs
{
    public class MainViewModel : INotifyPropertyChanged
    {
        Project selectedProject;

        public MainViewModel()
        {
            Manager = DataManager.Instance();
            UI = UserDialogs.Instance;
            AddProjectCommand = new Command(AddProject);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #region Properties

        public ICommand AddProjectCommand { get; protected set; }

        public DataManager Manager { get; set; }

        public IUserDialogs UI { get; set; }

        public string AppName
        {
            get { return Manager.AppName; }

            set {
                if (Manager.AppName != value)
                {
                    Manager.AppName = value;
                    OnPropertyChanged("AppName");
                }
            }
        }

        public Project SelectedProject
        {
            get { return selectedProject; }

            set {
                selectedProject = value;

                if (selectedProject == null)
                {
                    OnPropertyChanged("SelectedProject");
                    return;
                }

                App.NaviPage.Navigation.PushAsync(new TasksPage(selectedProject.Id));
                SelectedProject = null;
            }
        }

        public ObservableCollection<Project>ProjectList
        {
            get { return Manager.ProjectList; }

            set {
                if (Manager.ProjectList != value)
                {
                    Manager.ProjectList = value;
                    OnPropertyChanged("ProjectList");
                }
            }
        }

        public string Version
        {
            get { return Manager.Version; }

            set {
                if (Manager.Version != value)
                {
                    Manager.Version = value;
                    OnPropertyChanged("Version");
                }
            }
        }

        public string AboutTitle
        {
            get { return Manager.AboutTitle; }

            set {
                if (Manager.AboutTitle != value)
                {
                    Manager.AboutTitle = value;
                    OnPropertyChanged("AboutTitle");
                }
            }
        }

        #endregion

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        async void AddProject()
        {
            Debug.WriteLine("Clicked");
            var resault = await UI.PromptAsync(new PromptConfig
                {
                    Title = "Add Project",
                    InputType = InputType.Default,
                    OkText = "Add",
                    CancelText = "Cancel"
                });
            if (resault.Ok)
            {
                if (resault.Text.Length == 0)
                {
                    await UI.AlertAsync("Please, write project name", "Warning", "Ok");
                    AddProject();
                    return;
                }

                var newProject = new Project(resault.Text);
                Manager.AddProject(newProject);
                int newId = 0;
                foreach (Project item in Manager.DataBase.Table<Project>().ToList())
                {
                    if (item.Id > newId)
                    {
                        newId = item.Id;
                    }
                } 

                newProject.Id = newId;
                MainPageApplication.CurrentListView.BeginRefresh();
            }
        }
    }
}