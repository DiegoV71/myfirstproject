﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;

using Acr.UserDialogs;
using Xamarin.Forms;

namespace TODOs
{
    public class TaskViewModel : INotifyPropertyChanged
    {
        Task selectedTask;

        public TaskViewModel()
        {
            Manager = DataManager.Instance();
            UI = UserDialogs.Instance;
            ShowMenuCommand = new Command(ShowMenu);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #region Properties

        public DataManager Manager { get; set; }

        public IUserDialogs UI { get; set; }

        public ICommand ShowMenuCommand { get; protected set; }

        public Task SelectedTask
        {
            get { return selectedTask; }

            set {
                selectedTask = value;

                if (selectedTask == null)
                {
                    OnPropertyChanged("SelectedTask");
                    return;
                }

                App.NaviPage.Navigation.PushAsync(new EditPage(SelectedTask.Id));
                SelectedTask = null;
            }
        }

        public ObservableCollection<Task>TasksList
        {
            get { return Manager.TaskList; }

            set {
                if (Manager.TaskList != value)
                {
                    Manager.TaskList = value;
                    OnPropertyChanged("TasksList");
                }
            }
        }

        #endregion

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        void ShowMenu()
        {
            UI.ActionSheet(
                new ActionSheetConfig
                {
                    Title = "Project menu",
                    Options =
                    {
                        new ActionSheetOption("Add task", delegate
                            {
                                AddTask();
                            })
                    },
                    Cancel = new ActionSheetOption("Cancel"),
                    Destructive = new ActionSheetOption("Delete project", delegate
                        {
                            DeleteProject();
                        })
                });
        }

        void DeleteProject()
        {
            Manager.DeleteProject(StateMachine.CurrentProject.Id);
            StateMachine.CurrentProject = null;
            MainPageApplication.CurrentListView.BeginRefresh();
            App.NaviPage.PopAsync();
        }

        void AddTask()
        {
            App.NaviPage.PushAsync(new EditPage());
        }
    }
}