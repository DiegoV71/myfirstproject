﻿using System.Diagnostics;
using Xamarin.Forms;

namespace TODOs
{
    public partial class MainPageApplication : ContentPage
    {
        DataManager manager = DataManager.Instance();

        public static ListView CurrentListView { get; private set; }

        public MainPageApplication()
        {
            InitializeComponent();
            BindingContext = new MainViewModel();
            Title = "TODOs";
            CurrentListView = ProjectListView;
            ProjectListView.IsPullToRefreshEnabled = true;
            ProjectListView.Refreshing += (sender, e) =>
            {
                (sender as ListView).IsRefreshing = true;   
                Debug.WriteLine("Refresh");
                manager.ReLoadProjectList();
                ProjectListView.ItemsSource = manager.ProjectList;
                (sender as ListView).EndRefresh(); 
            };
        }
    }
}