﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace TODOs
{
    public partial class EditPage : ContentPage
    {
        TapGestureRecognizer iconRecognizer = new TapGestureRecognizer();
        DataManager manager = DataManager.Instance();

        public EditPage()
        { 
            InitializeComponent();
            IconConfigurator();
            DefaultViewConfigurator();
        }

        public EditPage(int taskId)
        {
            InitializeComponent();
            IconConfigurator();
            StateMachine.CurrentTask = manager.GetTask(taskId);
            ViewConfigurator();
        }

        void ViewConfigurator()
        {   
            var deleteToolbarItem = new ToolbarItem
            {
                Text = "Delete"      
            };

            var updateToolbarItem = new ToolbarItem
            {
                Text = "Update"
            };

            updateToolbarItem.SetBinding(ToolbarItem.CommandProperty, new Binding("UpdateTaskCommand"));
            deleteToolbarItem.SetBinding(ToolbarItem.CommandProperty, new Binding("DeleteTaskCommand"));

            ToolbarItems.Add(deleteToolbarItem);
            ToolbarItems.Add(updateToolbarItem);

            datePicker.Items.Add(StateMachine.CurrentTask.Date.ToString());
            datePicker.SelectedIndex = 0;

            typePicker.Items.Add("Select type of Task");
            typePicker.Items.Add("Bug");
            typePicker.Items.Add("Feature");
            typePicker.SelectedIndexChanged += TypeSelected;

            BindingContext = new EditViewModel(StateMachine.CurrentTask.Id);
        }

        void DefaultViewConfigurator()
        {
            BindingContext = new EditViewModel();

            var createToolbarItem = new ToolbarItem
            {
                Text = "Create",
            };

            createToolbarItem.SetBinding(ToolbarItem.CommandProperty, new Binding("CreateTaskCommand"));

            ToolbarItems.Add(createToolbarItem);

            descEditor.Text = "My new Task!";
            descEditor.BackgroundColor = Theme.BlueColor;

            datePicker.Items.Add(DateTime.Now.ToString());
            datePicker.SelectedIndex = 0;

            typePicker.Items.Add("Select type of Task");
            typePicker.Items.Add("Bug");
            typePicker.Items.Add("Feature");
            typePicker.SelectedIndex = 0;
            typePicker.SelectedIndexChanged += TypeSelected;

            statusPicker.Items.Add("Select type of Task");
            statusPicker.SelectedIndex = 0;
            statusPicker.IsEnabled = false;
        }

        void IconConfigurator()
        {
            redIcon.TextColor = Theme.RedColor;
            greenIcon.TextColor = Theme.GreenColor;
            blueIcon.TextColor = Theme.BlueColor;

            redIcon.GestureRecognizers.Add(iconRecognizer);
            greenIcon.GestureRecognizers.Add(iconRecognizer);
            blueIcon.GestureRecognizers.Add(iconRecognizer);

            iconRecognizer.Tapped += IconTapped;
        }

        void IconTapped(object sender, EventArgs e)
        {
            var color = ((IconView)sender).TextColor;
            descEditor.BackgroundColor = color;
        }

        void TypeSelected(object sender, EventArgs e)
        {
            var id = ((Picker)sender).SelectedIndex;
            switch (id)
            {
                case 1:
                    {
                        LoadBugStatuses();
                        break;
                    }

                case 2:
                    {
                        LoadFeatureStatuses();
                        break;
                    }

                case 0:
                    {
                        LoadDefaultStatuses();
                        break;
                    }
            }
        }

        void LoadBugStatuses()
        {
            statusPicker.Items.Clear();
            statusPicker.Items.Add("Select status of Bug");
            statusPicker.Items.Add("New Bug");
            statusPicker.Items.Add("Fixed Bug");
            statusPicker.SelectedIndex = 0;
            statusPicker.IsEnabled = true;
        }

        void LoadFeatureStatuses()
        {
            statusPicker.Items.Clear();
            statusPicker.Items.Add("Select status of Feature");
            statusPicker.Items.Add("New Feature");
            statusPicker.Items.Add("Implemented Feature");
            statusPicker.SelectedIndex = 0;
            statusPicker.IsEnabled = true;
        }

        void LoadDefaultStatuses()
        {
            statusPicker.Items.Clear();
            statusPicker.Items.Add("Select type of Task");
            statusPicker.SelectedIndex = 0;
            statusPicker.IsEnabled = false;
        }
    }
}
