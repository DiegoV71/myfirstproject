﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace TODOs
{
    public partial class TasksPage : ContentPage
    {
        DataManager manager = DataManager.Instance();

        public TasksPage(int projectID)
        {
            InitializeComponent();
            CurrentListView = TaskListView;
            StateMachine.CurrentProject = manager.GetProject(projectID);
            Title = string.Format("{0}'s tasks", manager.GetProjectname(projectID));

            Debug.WriteLine("#startLoad");
            manager.LoadTaskList(projectID);
            BindingContext = new TaskViewModel();
            foreach (Task item in manager.TaskList)
            {
                Debug.WriteLine(item.ToString());
            }

            Debug.WriteLine("#endLoad");

            CurrentListView.SeparatorVisibility = SeparatorVisibility.None;
            CurrentListView.IsPullToRefreshEnabled = true;
            CurrentListView.Refreshing += (object sender, EventArgs e) =>
            {
                (sender as ListView).IsRefreshing = true;   
                Debug.WriteLine("Refresh");
                manager.LoadTaskList(projectID);
                CurrentListView.ItemsSource = manager.TaskList;
                (sender as ListView).EndRefresh(); 
            };
        }

        public static ListView CurrentListView { get; private set; }
    }
}
