﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace TODOs
{
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {            
            InitializeComponent();
            this.Title = "About";
            this.BindingContext = new MainViewModel();

            LinkButton.Clicked += (object sender, EventArgs e) =>
            {
                Debug.WriteLine("Link Clicked");
                Device.OpenUri(new Uri(LinkButton.Text));
            };
        }       
    }
}
