﻿using System;

namespace TODOs
{
    public static class StateMachine
    {
        /// <summary>
        /// Gets or sets the current project.
        /// </summary>
        /// <value>The project.</value>
        public static Project CurrentProject { get; set; }

        /// <summary>
        /// Gets or sets the current task.
        /// </summary>
        /// <value>The task.</value>
        public static Task CurrentTask { get; set; }
    }
}
