﻿using Xamarin.Forms;

namespace TODOs
{
    public static class Theme
    {
        static Color blueColor = Color.FromHex("3498D8");
        static Color redColor = Color.FromHex("E74C3C");
        static Color greenColor = Color.FromHex("2eCC71");
        static Color grayColor = Color.FromHex("BDC3C7");
        static Color сyanColor = Color.FromHex("E0F7FA");

        public static Color СyanColor
        {
            get { return сyanColor; }
        }

        public static Color BlueColor
        {
            get { return blueColor; }
        }

        public static Color RedColor
        {
            get { return redColor; }
        }

        public static Color GreenColor
        {
            get { return greenColor; }
        }

        public static Color GrayColor
        {
            get { return grayColor; }
        }
    }
}