﻿using System;
using SQLite;

namespace TODOs
{
    public class Bug : Task
    {
        public Bug()
        { 
            Type = Types.bug;
        }

        [Ignore]
        public BugStatuses Status
        {
            get { return  (BugStatuses)Enum.Parse(typeof(BugStatuses), TaskStatus); }
            set { TaskStatus = value.ToString(); }
        }


    }
}