﻿using System;
using SQLite;
using Xamarin.Forms;

namespace TODOs
{
    [Table("Tasks")]
    public class Task
    {
        string description;
        DateTime date;
        Colors colorTask;
        Types typeTask;
        int id;
        int parent;

        public Task()
        {
            Date = DateTime.Now;
        }

        #region Enums

        /// <summary>
        /// Types task.
        /// </summary>
        public enum Types
        {
            /// <summary>
            /// The bug.
            /// </summary>
            bug,

            /// <summary>
            /// The feature.
            /// </summary>
            feature,

            /// <summary>
            /// The none.
            /// </summary>
            none
        }

        /// <summary>
        /// Colors task.
        /// </summary>
        public enum Colors
        {
            /// <summary>
            /// The red.
            /// </summary>
            Red,

            /// <summary>
            /// The green.
            /// </summary>
            Green,

            /// <summary>
            /// The blue.
            /// </summary>
            Blue,

            /// <summary>
            /// The none.
            /// </summary>
            none
        }

        /// <summary>
        /// Type of bug statuses
        /// </summary>
        public enum BugStatuses
        {
            /// <summary>
            /// The new bug.
            /// </summary>
            newBug,

            /// <summary>
            /// The fixed bug.
            /// </summary>
            fixedBug
        }

        /// <summary>
        /// Feature statuses.
        /// </summary>
        public enum FeatureStatuses
        {
            /// <summary>
            /// The new feature.
            /// </summary>
            newFeature,

            /// <summary>
            /// The implemented feature.
            /// </summary>
            Implemented
        }

        #endregion

        #region Properties

        [Column("_parent")]
        public int Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Column("_description")]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [Column("_color")]
        public Colors Color
        {
            get { return colorTask; }
            set { colorTask = value; }
        }            

        [Column("_date")]
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        [Column("_status")]
        public string TaskStatus { get; set; }

        [Column("_type")]
        public Types Type
        {
            get { return typeTask; }
            protected set { typeTask = value; }
        }

        #endregion

        public Enum GetStatus()
        {
            switch (Type)
            {
                case Types.bug:
                    {
                        return (BugStatuses)Enum.Parse(typeof(BugStatuses), TaskStatus);
                    }

                case Types.feature:
                    {
                        return (FeatureStatuses)Enum.Parse(typeof(FeatureStatuses), TaskStatus);
                    }

                default: 
                    {
                        return null;
                    }
            }
        }

        public T GetStatus<T>()
        {
            return (T)Enum.Parse(typeof(T), TaskStatus);
        }

        public override string ToString()
        {
            return string.Format("[{0}] {1} | Desc: {2} | Date: {3} | Color: {4} | Type: {5} | Status: {6} ", Parent, Id, Description, Date, Color, Type, GetStatus());
        }
    }
}