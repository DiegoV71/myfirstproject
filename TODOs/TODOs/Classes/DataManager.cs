﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using SQLite;
using Xamarin.Forms;

namespace TODOs
{
    public class DataManager
    {
        #region Private Fields

        static SQLiteConnection dataBase;
        static ObservableCollection<Project> projectList = new ObservableCollection<Project>();
        static ObservableCollection<Task> taskList = new ObservableCollection<Task>();
        static DataManager instance;
        static string appName = "TODOs";
        static string version = "1.0.0";
        static string aboutTitle = "About";

        #endregion

        DataManager()
        {
            dataBase = DependencyService.Get<ISQLite>().GetConnection();
            DataBase.CreateTable<Project>();
            DataBase.CreateTable<Task>();
            LoadProjectList();

            GetData();
        }

        #region Properties

        public SQLiteConnection DataBase
        {
            get { return dataBase; }
        }

        public string AppName
        {
            get { return appName; }
            set { appName = value; }
        }

        public string Version
        {
            get { return version; }
            set { version = value; }
        }

        public string AboutTitle
        {
            get { return aboutTitle; }
            set { aboutTitle = value; }
        }

        public ObservableCollection<Project> ProjectList
        {
            get { return projectList; }
            set { projectList = value; }
        }

        public ObservableCollection<Task> TaskList
        {
            get { return taskList; }
            set { taskList = value; }
        }

        #endregion

        public static DataManager Instance()
        {
            if (instance == null)
            {
                instance = new DataManager();
            }

            return instance;
        }

        public string GetProjectname(int id)
        {
            return GetProject(id).Name;
        }

        public void LoadProjectList()
        {
            ProjectList = new ObservableCollection<Project>(GetProjects().OrderBy(i => i.Name));
        }

        public void ReLoadProjectList()
        {
            ProjectList = new ObservableCollection<Project>(GetProjects().OrderBy(i => i.Name));
        }

        public void LoadTaskList(int id)
        {            
            List<Task> bugs = GetTasks(id).Where(i => (i.Type == Task.Types.bug) &&
                                  (i.TaskStatus != Task.BugStatuses.fixedBug.ToString())).ToList();
            List<Task> features = GetTasks(id).Where(i => (i.Type == Task.Types.feature) &&
                                      (i.TaskStatus != Task.FeatureStatuses.Implemented.ToString())).ToList();
            List<Task> isRealise = GetTasks(id).Where(i => (i.TaskStatus == Task.BugStatuses.fixedBug.ToString()) ||
                                       (i.TaskStatus == Task.FeatureStatuses.Implemented.ToString())).ToList();
            List<Task> sortedList = new List<Task>();
            sortedList.AddRange(bugs);
            sortedList.AddRange(features);
            sortedList.AddRange(isRealise);
            TaskList = new ObservableCollection<Task>(sortedList);
        } 

        #region SQL Methods

        public IEnumerable<Project> GetProjects()
        {
            return (from i in DataBase.Table<Project>()
                select i).ToList();
        }

        public IEnumerable<Task> GetTasks(int idParent)
        {
            return (from task in DataBase.Table<Task>()
                where task.Parent == idParent
                select task).ToList();
        }

        public Project GetProject(int id)
        {
            return DataBase.Get<Project>(id);
        }

        public Task GetTask(int id)
        {
            return DataBase.Get<Task>(id);
        }

        public int DeleteProject(int id)
        {
            return DataBase.Delete<Project>(id);
        }

        public int DeleteTask(int id)
        {
            return DataBase.Delete<Task>(id);
        }

        public int SaveTask(Task item)
        {
            DataBase.Update(item);
            return item.Id;
        }

        public int AddTask(Task item)
        {
            return DataBase.Insert(item);
        }

        public int AddProject(Project item)
        {
            return DataBase.Insert(item);
        }

        #endregion

        async void GetData()
        {
            var client = new RestClient("http://localhost:3000");
            var getRequest = new RestRequest();
            var testRequest = new RestRequest();

            try
            {
                var testResponse = await client.Execute(testRequest);
            }
            catch (Exception e)
            {
                Debug.WriteLine(string.Format("Connection to server is failed. Status: {0}", e.Message));
                return;                
            }

            var message = new { message = "My First Post! JSON! " };

            var postRequest = new RestRequest(Method.POST);
            postRequest.AddBody(message);

            await client.Execute(postRequest);
            Debug.WriteLine("Сделали POST запрос");

            var response = await client.Execute(getRequest);

            var content = response.Content;

            Debug.WriteLine(content); 
        }
    }
}