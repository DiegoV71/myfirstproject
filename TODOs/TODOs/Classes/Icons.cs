﻿namespace TODOs
{
    public static class Icons
    {
        const string feature = "\uf0eb";
        const string bug = "\uf188";
        const string arrow = "\uf0a9";
        const string ok = "\uf00c";

        public static string Ok
        {
            get { return ok; }
        }

        public static string Feature
        {
            get { return feature; }
        }

        public static string Bug
        {
            get { return bug; }
        }

        public static string Arrow
        {
            get { return arrow; }
        }
    }
}