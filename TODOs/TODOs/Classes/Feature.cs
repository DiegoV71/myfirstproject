﻿using System;
using SQLite;

namespace TODOs
{
    public class Feature : Task
    {
        public Feature()
        {
            Type = Types.feature;
        }
        [Ignore]
        public FeatureStatuses Status
        {
            get { return (FeatureStatuses)Enum.Parse(typeof(FeatureStatuses), TaskStatus); }
            set { TaskStatus = value.ToString(); }
        }
    }
}