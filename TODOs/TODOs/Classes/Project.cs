﻿using SQLite;

namespace TODOs
{
    [Table("Projects")]
    public class Project
    {
        string name;
        int id;

        public Project(string name = "Project Name")
        {
            Name = name;
        }

        public Project()
        {
        }

        [PrimaryKey, AutoIncrement, Column("_id")] 
        public int Id {
            get {
                return id;
            }

            set {
                id = value;
            }
        }

        [Column("_name")]
        public string Name {
            get {
                return name;
            }

            set {
                name = value;
            }
        }
    }
}
