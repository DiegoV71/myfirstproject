﻿using Xamarin.Forms;

namespace TODOs
{
    public partial class TaskView : ContentView
    {
        public static readonly BindableProperty ColorProperty =
            BindableProperty.Create("Color",
                typeof(Task.Colors),
                typeof(TaskView),
                Task.Colors.none,
                BindingMode.OneWay,
                null,
                (bindable, oldValue, newValue) =>
                {
                    var thisView = (TaskView)bindable;
                    switch ((Task.Colors)newValue)
                    {
                        case Task.Colors.Red:
                            {
                                thisView.taskRow.BackgroundColor = Theme.RedColor;
                                break;
                            }

                        case Task.Colors.Blue:
                            {
                                thisView.taskRow.BackgroundColor = Theme.BlueColor;
                                break;
                            }

                        case Task.Colors.Green:
                            {
                                thisView.taskRow.BackgroundColor = Theme.GreenColor;
                                break;
                            }

                        default:
                            {
                                thisView.taskRow.BackgroundColor = Theme.GrayColor;
                                break;
                            }
                    }
                });

        public static readonly BindableProperty StatusProperty =
            BindableProperty.Create("Status",
                typeof(string),
                typeof(TaskView),
                "none",
                BindingMode.OneWay,
                null,
                (bindable, oldValue, newValue) =>
                {
                    var thisView = (TaskView)bindable;
                    if ((string)newValue == Task.BugStatuses.fixedBug.ToString())
                    {
                        thisView.taskRow.BackgroundColor = Theme.GrayColor;
                        thisView.TypeIcon.Icon = Icons.Ok;
                    }

                    if ((string)newValue == Task.FeatureStatuses.Implemented.ToString())
                    {
                        thisView.taskRow.BackgroundColor = Theme.GrayColor;
                        thisView.TypeIcon.Icon = Icons.Ok;
                    }
                });

        public static readonly BindableProperty TypeProperty =
            BindableProperty.Create("Type",
                typeof(Task.Types),
                typeof(TaskView),
                Task.Types.none,
                BindingMode.OneWay,
                null,
                (bindable, oldValue, newValue) =>
                {
                    var thisView = (TaskView)bindable;
                    switch ((Task.Types)newValue)
                    {
                        case Task.Types.bug:
                            {
                                thisView.TypeIcon.Icon = Icons.Bug;
                                break;
                            }

                        case Task.Types.feature:
                            {
                                thisView.TypeIcon.Icon = Icons.Feature;
                                break;
                            }
                    }
                });

        public TaskView()
        {
            InitializeComponent();
        }
    }
}
