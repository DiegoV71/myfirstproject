﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace TODOs
{
    public partial class ItemProjectView : ContentView
    {
    //    private ScrollView scroll = new ScrollView();

        public ItemProjectView()
        {
            InitializeComponent();
        }

        //public ItemProjectView(string name)
        //{
        //    InitializeComponent();
        //    this.ProjectName = name;
        //}


        private void OnPlusClicked(object sender, EventArgs arg) {
            App.Current.MainPage.DisplayAlert("Список задач", "Открыть список задач невозможно", "Закрыть");
        }

        public string ProjectName
        { 
            set
            { 
                projectNameLabel.Text = value;
             
            }
            get
            { 
                return projectNameLabel.Text;
            }
        }
    }
}

