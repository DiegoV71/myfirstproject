﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace TODOs
{
    public class App : Application
    {
        DataManager manager;
        static NavigationPage naviPage;

        public App()
        { 
            manager = DataManager.Instance();
            naviPage = new NavigationPage(new MainPageApplication()) { Title = "TODOs", Icon = Device.OnPlatform<FileImageSource>("todoImg.png","","") };
//            MainPage = new EditPage();
            MainPage = new TabbedPage
            {
                Children =
                {
                    NaviPage,
                    new AboutPage
                    { 
                                Title = "About",
                                Icon = Device.OnPlatform<FileImageSource>("aboutImg.png","","")
                    }
                },
                BarBackgroundColor = Theme.BlueColor,
                BarTextColor = Color.White
            };
        }

        public static NavigationPage NaviPage
        {
            get { return naviPage; }
        }

        protected override void OnStart()
        {                
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}