﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace TODOs
{
    public partial class ColorView : ContentView
    {
        ColorTypeConverter colorTypeConv = new ColorTypeConverter();
        public ColorView()
        {
            InitializeComponent();
        }

        public string ColorName {
            set {
                // Set the name.
                colorName.Text = value;
               
                // Get the actual Color and set the other views.
                Color color = Color.Accent;// (Color)colorTypeConv.ConvertFrom(colorName.Text);
                colorBox.Color = color;
                colorName.Text += String.Format(" {0:X2}-{1:X2}-{2:X2}",
                    (int)(255 * color.R),
                    (int)(255 * color.G),
                    (int)(255 * color.B));
            }
            get {
                return colorName.Text;
                }
        }
    }
}

